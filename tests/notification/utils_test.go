package notification

import (
	. "notification_service/notification"
	"testing"
	"time"
)

func TestIsWithinRateLimit(t *testing.T) {

	tests := []struct {
		name           string
		notifications  []time.Time
		limit          RateLimit
		expectedResult bool
	}{
		{
			name:           "No notifications sent",
			notifications:  []time.Time{},
			limit:          RateLimit{MaxCount: 1, Interval: 1 * time.Hour},
			expectedResult: true,
		},
		{
			name: "Under limit",
			notifications: []time.Time{
				time.Now().Add(-30 * time.Minute),
			},
			limit:          RateLimit{MaxCount: 2, Interval: 1 * time.Hour},
			expectedResult: true,
		},
		{
			name: "At limit",
			notifications: []time.Time{
				time.Now().Add(-30 * time.Minute),
				time.Now().Add(-20 * time.Minute),
			},
			limit:          RateLimit{MaxCount: 2, Interval: 1 * time.Hour},
			expectedResult: true,
		},
		{
			name: "Over limit",
			notifications: []time.Time{
				time.Now().Add(-30 * time.Minute),
				time.Now().Add(-20 * time.Minute),
				time.Now().Add(-10 * time.Minute),
			},
			limit:          RateLimit{MaxCount: 2, Interval: 1 * time.Hour},
			expectedResult: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsWithinRateLimit(tt.notifications, tt.limit); got != tt.expectedResult {
				t.Errorf("IsWithinRateLimit() = %v, want %v", got, tt.expectedResult)
			}
		})
	}
}
