package notification

import (
	. "notification_service/notification"
	"testing"
	"time"
)

func TestSendNotification(t *testing.T) {
	rateLimits := map[NotificationType]RateLimit{
		StatusUpdate: {MaxCount: 2, Interval: 1 * time.Minute},
	}
	service := NewNotificationService(rateLimits)

	err := service.SendNotification("test@test.com", StatusUpdate)
	if err != nil {
		t.Errorf("SendNotification() error = %v, wantErr %v", err, false)
	}
}

func TestRateLimit(t *testing.T) {
	rateLimits := map[NotificationType]RateLimit{
		StatusUpdate: {MaxCount: 1, Interval: 1 * time.Minute},
	}
	service := NewNotificationService(rateLimits)

	err := service.SendNotification("test@test.com", StatusUpdate)
	if err != nil {
		t.Errorf("First notification should not hit rate limit")
	}

	err = service.SendNotification("test@test.com", StatusUpdate)
	if err == nil {
		t.Errorf("Second notification should hit rate limit")
	}
}

func TestAddNotificationType(t *testing.T) {
	service := NewNotificationService(make(map[NotificationType]RateLimit))

	newType := NotificationType("CustomAlert")
	newRateLimit := RateLimit{MaxCount: 2, Interval: 1 * time.Hour}
	service.AddNotificationType(newType, newRateLimit)

	err := service.SendNotification("test@test.com", newType)
	if err != nil {
		t.Errorf("SendNotification() error = %v, wantErr %v", err, false)
	}
}
