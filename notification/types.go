package notification

import (
	"time"
)

type NotificationType string

const (
	StatusUpdate NotificationType = "Status"
	DailyNews    NotificationType = "News"
	Marketing    NotificationType = "Marketing"
)

type RateLimit struct {
	MaxCount int
	Interval time.Duration
}
