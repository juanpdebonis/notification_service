package notification

import (
	"errors"
	"sync"
	"time"
)

type NotificationService struct {
	rateLimits    map[NotificationType]RateLimit
	notifications map[string]map[NotificationType][]time.Time
	mu            sync.Mutex
}

func NewNotificationService(rateLimits map[NotificationType]RateLimit) *NotificationService {
	return &NotificationService{
		rateLimits:    rateLimits,
		notifications: make(map[string]map[NotificationType][]time.Time),
	}
}

func (s *NotificationService) SendNotification(recipient string, notificationType NotificationType) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if s.notifications[recipient] == nil {
		s.notifications[recipient] = make(map[NotificationType][]time.Time)
	}

	s.notifications[recipient][notificationType] = append(s.notifications[recipient][notificationType], time.Now())

	if !s.isAllowedToSend(recipient, notificationType) {
		return errors.New("rate limit exceeded")
	}

	return nil
}

func (s *NotificationService) isAllowedToSend(recipient string, notificationType NotificationType) bool {
	limit, ok := s.rateLimits[notificationType]
	if !ok {
		return true
	}

	notifications := s.notifications[recipient][notificationType]
	return IsWithinRateLimit(notifications, limit)
}

func (s *NotificationService) AddNotificationType(notificationType NotificationType, rateLimit RateLimit) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.rateLimits[notificationType] = rateLimit
}
