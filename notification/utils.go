package notification

import "time"

func IsWithinRateLimit(notifications []time.Time, limit RateLimit) bool {
	now := time.Now()
	count := 0

	for _, nTime := range notifications {
		if now.Sub(nTime) <= limit.Interval {
			count++
		}
	}

	return count <= limit.MaxCount
}
