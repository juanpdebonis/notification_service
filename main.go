package main

import (
	"fmt"
	"notification_service/notification"
	"time"
)

func main() {

	rateLimits := map[notification.NotificationType]notification.RateLimit{
		notification.StatusUpdate: {MaxCount: 2, Interval: 1 * time.Minute},
		notification.DailyNews:    {MaxCount: 1, Interval: 24 * time.Hour},
		notification.Marketing:    {MaxCount: 3, Interval: 1 * time.Hour},
	}

	service := notification.NewNotificationService(rateLimits)

	specialEventType := notification.NotificationType("SpecialEvent")
	service.AddNotificationType(specialEventType, notification.RateLimit{MaxCount: 2, Interval: 2 * time.Hour})

	for i := 0; i < 3; i++ {
		err := service.SendNotification("user@example.com", notification.StatusUpdate)
		if err != nil {
			fmt.Printf("Failed to send Status Update %d: %v\n", i+1, err)
		} else {
			fmt.Printf("Sent Status Update %d successfully\n", i+1)
		}
	}

	for i := 0; i < 2; i++ {
		err := service.SendNotification("user@example.com", notification.DailyNews)
		if err != nil {
			fmt.Printf("Failed to send Daily News %d: %v\n", i+1, err)
		} else {
			fmt.Printf("Sent Daily News %d successfully\n", i+1)
		}
	}

	for i := 0; i < 4; i++ {
		err := service.SendNotification("user@example.com", notification.Marketing)
		if err != nil {
			fmt.Printf("Failed to send Marketing %d: %v\n", i+1, err)
		} else {
			fmt.Printf("Sent Marketing %d successfully\n", i+1)
		}
	}

	for i := 0; i < 3; i++ {
		err := service.SendNotification("user@example.com", specialEventType)
		if err != nil {
			fmt.Printf("Failed to send SpecialEvent %d: %v\n", i+1, err)
		} else {
			fmt.Printf("Sent SpecialEvent %d successfully\n", i+1)
		}
	}

}
