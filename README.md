# Notification Service

## Getting Started

### Prerequisites

- [Go](https://golang.org/dl/) (version 1.21 or later)
- [Docker](https://www.docker.com/products/docker-desktop) (if running via Docker)

### Installing and Running

#### Running Locally

1. **Clone the repository**:

   ```bash
    git clone https://gitlab.com/juanpdebonis/notification_service
    cd notification_service
   ```

2. **Download the dependencies**:
   ```bash
    go mod tidy
   ```

#### Run with docker

1. **Build**:
   ```bash
    docker-compose build
   ```

2. **Run**:
   ```bash
    docker-compose up
   ```

### Running the tests
#### Testing the Application

#### Run the following command to execute the unit tests:

```bash
  go test ./... -v -coverpkg=./...
```


### Testing with Docker

When you build the Docker image using docker-compose build, the tests are automatically executed as part of the Docker build process. If any test fails, the Docker build will fail.