FROM golang:latest as build

WORKDIR /go/src/app

COPY . .

RUN go mod tidy

RUN go fmt ./...

RUN go test ./... -v -coverpkg=./... || exit 1

RUN go build -o main .

FROM debian:buster

WORKDIR /root/

COPY --from=build /go/src/app/main .

ENTRYPOINT ["./main"]
